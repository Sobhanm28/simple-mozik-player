import 'dart:io';

import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:ddd_design/MusicStatus.dart';
import 'package:ddd_design/domain/DownloadService.dart';
import 'package:ddd_design/domain/MusicService.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:responsive_widgets/responsive_widgets.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  MusicService _musicService;
  MusicStatus _musicStatus = MusicStatus.Stoped;
  Duration _musicPosition = Duration(seconds: 0);
  PlayingAudio _musicDuration;
  @override
  void initState() {
    super.initState();
    initialize();
  }

  initialize() async {
    _musicService = await MusicService.instance();
    await _musicService.play();
    getMusicStatusFromService();
    getMusicDurationFromService();
    getMusicPositionFromService();
  }

  getMusicDurationFromService() {
    _musicService.getMusicDuration().listen((duration) {
      setState(() {
        _musicDuration = duration;
      });
    });
  }

  getMusicPositionFromService() {
    _musicService.getMusicPosition().listen((position) {
      setState(() {
        _musicPosition = position;
      });
    });
  }

  getMusicStatusFromService() {
    _musicService.getMusicStatus().listen((status) {
      setState(() {
        _musicStatus = status;
      });
    });
  }

  playerButton() {
    switch (_musicStatus) {
      case MusicStatus.Loading:
        return Container(
          child: CircularProgressIndicator(),
        );
        break;
      case MusicStatus.Paused:
        return IconButton(
            icon: Icon(Icons.play_arrow),
            onPressed: () {
              _musicService.play();
            });
        break;
      case MusicStatus.Playing:
        return IconButton(
            icon: Icon(Icons.pause),
            onPressed: () {
              _musicService.pause();
            });
        break;
      case MusicStatus.Stoped:
        return Container(
          child: CircularProgressIndicator(),
        );
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 0.3,
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Center(
                child: playerButton(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(_musicPosition.toString().substring(3, 7)),
                  SliderTheme(
                    data: SliderTheme.of(context).copyWith(
                        thumbShape:
                            RoundSliderThumbShape(enabledThumbRadius: 2.5)),
                    child: Slider(
                      min: 0,
                      max: _musicDuration?.duration?.inMilliseconds
                              ?.toDouble() ??
                          0,
                      value: _musicPosition.inMilliseconds.toDouble(),
                      onChanged: (value) {},
                    ),
                  ),
                  Text(_musicDuration?.duration?.toString()?.substring(3, 7) ??
                      "0:00"),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
