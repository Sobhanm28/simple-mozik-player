import 'dart:async';

import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:ddd_design/domain/DownloadService.dart';

import '../MusicStatus.dart';

class MusicService {
  AssetsAudioPlayer _player;
  DownloadService _downloadService;
  static MusicService _instance;
  StreamController<MusicStatus> _musicStatusStream;

static Future<MusicService> instance() async {
    if(_instance==null){
      _instance= MusicService._privateCtor();
      await _instance.initialize();
    }
    return _instance;
  }

  initialize() async{
    _player = AssetsAudioPlayer();
    _downloadService = await DownloadService.instance();
    _musicStatusStream = StreamController();

  }

  MusicService._privateCtor();

  void setMusicStatus(MusicStatus status) => _musicStatusStream.add(status);

  Stream getMusicPosition()=>_player.currentPosition;

  Stream getMusicDuration()=>_player.onReadyToPlay;

  Stream getMusicStatus() => _musicStatusStream.stream;

  Future<void> play() async {

    String audioFile = await _downloadService.download();
    setMusicStatus(MusicStatus.Playing);
  

    _player.open(Audio.file(audioFile));
  }

  void pause(){
    setMusicStatus(MusicStatus.Paused);
    _player.pause();
  }

  void stop() {
    setMusicStatus(MusicStatus.Stoped);

    _player.stop();

  }
}
