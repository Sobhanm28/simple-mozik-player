import 'dart:async';
import 'dart:io';

import 'package:ddd_design/MusicStatus.dart';
import 'package:ddd_design/domain/MusicService.dart';
import 'package:dio/dio.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class DownloadService {
  Dio dio;
  Directory saveDir;
  MusicService _musicService;
  static DownloadService _instance;

  static Future<DownloadService> instance() async {
    if (_instance == null) {
      _instance = DownloadService._privateCtor();
      await _instance.initialize();
    }
    return _instance;
  }

  DownloadService._privateCtor();

  Future initialize() async {
    saveDir = await getExternalStorageDirectory();
    if (!Directory(getAudioFolderDir()).existsSync()) {
      Directory(getAudioFolderDir()).createSync();
    }

    dio = Dio();
    dio.interceptors.add(LogInterceptor());
    _musicService=await MusicService.instance();
  }

  String getSaveDir() => saveDir.path;
  String getAudioFolderDir() => join(getSaveDir(), "audio");
  String getAudioFileDir() => join(getAudioFolderDir(), "hamid_askari.mp3");

 

  Future<String> download() async {
    if (!File(getAudioFileDir()).existsSync()) {
      _musicService.setMusicStatus(MusicStatus.Loading);
      await dio.download(
          "http://dl1.tilarmusic.ir/media/music/Persian-Artists/Hamid%20Askari/Albums/Coma%203%20Album/Hamid-Askari-Man-Kojaye-Zendegitam.mp3",
          getAudioFileDir());
    }
    return getAudioFileDir();
  }
}
